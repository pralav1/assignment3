package com.wipro.testcases;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.wipro.testbase.TestBase;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class TC01 extends TestBase{
	WebDriver dr;
	
	@BeforeClass
	public void BeforeClass() {
		dr=new ChromeDriver();
	}
	
	@Test
	public void Test1() throws IOException, InterruptedException {
		Properties properties=new Properties();
		properties.load(new FileInputStream("C:\\Users\\PR20111915\\selenium\\Assignment33\\resources\\config\\Config.properties"));
		String url=properties.getProperty("url");
		dr.get(url);
		Thread.sleep(2500);
	}
	
	@Test
	public void Test2() throws InterruptedException {
		dr.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/a/i")).click();
		dr.findElement(By.linkText("Register")).click();
		Thread.sleep(2500);
	}
	
	@Test
	public void Test3() throws InterruptedException, BiffException, IOException {
		File f=new File("C:\\Users\\PR20111915\\selenium\\Assignment33\\resources\\testdata\\Registration.xls");
		Workbook workbook=Workbook.getWorkbook(f);
		Sheet sheet=workbook.getSheet(0);
		int col=sheet.getColumns();
		String[] str=new String[col];
		for(int i=0;i<col;i++) {
			Cell cell=sheet.getCell(i, 0);
			str[i]=cell.getContents();
		}
		dr.findElement(By.name("firstname")).sendKeys(str[0]);
		dr.findElement(By.name("lastname")).sendKeys(str[1]);
		dr.findElement(By.name("email")).sendKeys(str[2]);
		dr.findElement(By.name("telephone")).sendKeys(str[3]);
		dr.findElement(By.name("password")).sendKeys(str[4]);
		dr.findElement(By.name("confirm")).sendKeys(str[5]);
		Thread.sleep(2500);
	}
	
	@Test
	public void Test4() throws IOException, InterruptedException {
		dr.findElement(By.name("agree")).click();
		if(dr.findElement(By.name("agree")).isSelected()) {
			BufferedWriter writer = new BufferedWriter(new FileWriter("resources\\output_data\\output.txt"));
		    writer.write("Checkbox is checked.");
		    writer.close();
		}
		Thread.sleep(2500);
	}
	
	@Test
	public void Test5() throws IOException, InterruptedException {
		dr.findElement(By.xpath("//*[@id=\"content\"]/form/div/div/input[2]")).click();
		BufferedWriter writer = new BufferedWriter(new FileWriter("resources\\output_data\\output.txt", true));
	    writer.write("\n"+dr.findElement(By.xpath("//*[@id=\"content\"]/h1")).getText());
	    writer.close();
		File screenshotFile = ((TakesScreenshot)dr).getScreenshotAs(OutputType.FILE);
	    FileUtils.copyFile(screenshotFile, new File("C:\\Users\\PR20111915\\selenium\\Assignment33\\resources\\screenshots\\TC01_5.png"));
		Thread.sleep(2500);
	}
	
	@Test
	public void Test6() throws IOException, InterruptedException {
		dr.findElement(By.linkText("My Account")).click();
		Thread.sleep(2500);
	}
	
	@Test
	public void Test7() throws IOException, InterruptedException {
		dr.findElement(By.linkText("Logout")).click();
		BufferedWriter writer = new BufferedWriter(new FileWriter("resources\\output_data\\output.txt", true));
	    writer.write("\n"+dr.findElement(By.xpath("//*[@id=\"content\"]/h1")).getText());
	    writer.close();
		Thread.sleep(2500);
	}
	
	@AfterClass
	public void AfterClass() {
		dr.close();
	}
}
